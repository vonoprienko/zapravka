function FuelStationManager(name) {
    this.name = name;
    var fuelStation = null;

    this.setFuelStation = function (Station) {
        if (Station instanceof FuelStation) {
            fuelStation = Station;
        } else {
            throw 'You must put the object instance of FuelStation '
        }
    };

    this.refuelCar = function (car, typeFuel, fuelValue) {
        if (!fuelStation) {
            throw 'Hire me to the fuel station';
        }

        try {
            var refuel = fuelStation.refuel(car, typeFuel, fuelValue);
            if (refuel) {
                console.log(name + ' filled ' + car + ' using ' + fuelStation.getFuelName(typeFuel) + ' .');
            }

        } catch (e) {
            if (e.fuelValue) {
                console.error(e.description);
                this.refuelCar(car, typeFuel, e.fuelValue);
            } else {
                console.error(e)
            }
        }
    }
}