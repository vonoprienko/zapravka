/**
 * Represents cars class
 * @returns {Object}
 * @constructor
 */

function Car() {
    makes = ["Tesla", "Buick", "Cadillac", "Chevrolet", "Tesla"];
    models = ["S hetchback", "X crossover", "Runner", "Taxi", "Cammaro"];
    years = [2000, 1990, 2010, 2015, 2005];
    colors = ["red", "blue", "tan", "yellow", "white"];
    rand1 = Math.floor(Math.random() * makes.length);
    rand2 = Math.floor(Math.random() * models.length);
    rand3 = Math.floor(Math.random() * years.length);
    rand4 = Math.floor(Math.random() * colors.length);
    rand5 = Math.floor(Math.random() * 41) + 10;
    rand6 = Math.floor(Math.random() * 9) + 1;

    this.make = makes[rand1],
        this.model = models[rand2],
        this.year = years[rand3],
        this.color = colors[rand4],
        this.capacityTank = rand5,
        this.currentCapacityTank = rand6,
        this.mileage = 0


    this.toString = function () {
        return this.make + " " + this.model + " " + this.year;
    }
}