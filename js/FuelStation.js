/**
 * Represents fuel station class
 * @param name {string} defines brand of fuel station
 * @param limit {number} defines capacity of max capacity of fuel station.
 * @constructor
 */
function FuelStation(name, limit) {
    var fuelStationName = name;
    var contract = new fuelContracts(0, 0, 0, limit);

    this.addContract = function (fuelType) {
        contract.addFuel(fuelType);
        colorTrace("Add contract " + this.getFuelName(fuelType) + ".", "magenta")
    };

    this.fullTank = function tankCapacity(car, fuelType) {
        if (checkCapacityTank(car) > getFuelValue(fuelType)) {
            console.log("Sorry is not enough " + this.getFuelName(fuelType) + " " + Math.abs(getFuelValue(fuelType) - checkCapacityTank(car)) + " liters ! Turn later.");
            this.addContract(fuelType)
        } else {
            colorTrace(car + " filled " + this.getFuelName(fuelType) + " you up to a full tank.", "green");
            while (car.capacityTank > car.currentCapacityTank) {
                console.log('Full tank loading - ' + ++car.currentCapacityTank + ' ...');
                contract.subtract(fuelType, 1)
            }
            colorTrace("We filled you the full tank. Good luck!", "green");
        }
    };

    /**
     * Refueling car by fuel id and value
     * @param car {object} defines car
     * @param fuelType {object} defines type of fuel
     * @param fuelValue {number} defines how many need fuel for refueling car
     */
    this.refuel = function (car, fuelType, fuelValue) {
        console.log("Filling fuel 92 in " + car + " - " + fuelValue + " liters.");

        if ((checkCapacityTank(car) - fuelValue) >= 0) {
            switch (fuelType) {
                case 1:
                    if (fuelValue < contract.f92) {
                        car.currentCapacityTank += fuelValue;
                        contract.subtract(fuelType, fuelValue);
                        return true;
                    } else {
                        console.log("Sorry is not enough 92 gasoline = " + Math.abs(contract.f92 - fuelValue) + " liters ! Turn later.");
                        this.addContract(FUEL_TYPES.F92);
                        return false;
                    }
                case 2:
                    if (fuelValue < contract.f95) {
                        car.currentCapacityTank += fuelValue;
                        contract.subtract(fuelType, fuelValue);
                        return true;
                    } else {
                        console.log("Sorry is not enough 95 gasoline = " + Math.abs(contract.f95 - fuelValue) + " liters! Turn later.");
                        this.addContract(FUEL_TYPES.F95);
                        return false;
                    }
                case 3:
                    if (fuelValue < contract.fdiesel) {
                        car.currentCapacityTank += fuelValue;
                        contract.subtract(fuelType, fuelValue);
                        return true;
                    } else {
                        console.log("Sorry is not enough diesel gasoline = " + Math.abs(contract.fdiesel - fuelValue) + " liters! Turn later.");
                        this.addContract(FUEL_TYPES.FDIESEL);
                        return false;
                    }
                default :
                    throw ("We have some problem with refueling...");
            }
        } else {
            throw {
                description: 'You can not pour more than the capacity of the fuel tank. We will fill you a full tank.',
                fuelValue: checkCapacityTank(car)
            };
        }


    };

    function getFuelValue(fuelType) {
        switch (fuelType) {
            case 1:
                return contract.f92;
            case 2:
                return contract.f95;
            case 3:
                return contract.fdiesel;
            default :
                throw 'I do not know this kind of fuel! Sorry!'
        }
    }


    this.getFuelName = function (fuelType) {
        switch (fuelType) {
            case 1:
                return "92 petrol";
            case 2:
                return "95 petrol";
            case 3:
                return "diesel petrol";
            default :
                throw 'I do not know this kind of fuel! Sorry!'
        }
    }

    function checkCapacityTank(car) {
        return car.capacityTank - car.currentCapacityTank;
    }

    this.toString = function () {
        return fuelStationName;
    }
}