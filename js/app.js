(function () {
    var buildTime = 100;
    var progressBuild = 0;
    var g = setInterval(
        function () {
            if (progressBuild <= buildTime) {
                colorTrace("Progress build fuel station " + progressBuild + "%", "grey")
                if (progressBuild === buildTime) {
                    colorTrace("Well done!", "black")
                }
                progressBuild += 10;
            } else {
                clearInterval(g);
            }
        },
        500);
})();

setTimeout(function () {
    const limit = 1000;
    var Wog = new FuelStation("Wog", limit);

    var Manager = new FuelStationManager("Joshua");
    Manager.setFuelStation(Wog);

    var j = 1;
    var g = setInterval(function () {
        if (j == 10) {
            clearInterval(g)
        }

        var car = new Car();
        if (j % 2 === 1) {
            colorTrace("Next car >>>", "blue");
            Manager.refuelCar(
                car,
                Math.floor(Math.random() * 3) + 1,
                Math.floor(Math.random() * (car.capacityTank - car.currentCapacityTank)) + 1);
            return j++;
        }
        colorTrace("Next car >>>", "green");
        Wog.fullTank(
            car,
            Math.floor(Math.random() * 3) + 1
        );
        return j++;
    }, 6000);
}, 6000);

