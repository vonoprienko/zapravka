/**
 * Represents fuel id's
 * FUEL_TYPES {object of constants}
 */
var FUEL_TYPES = {
    F92: 1,
    F95: 2,
    FDIESEL: 3
};

/**
 * Represents refueling fuel station
 * @param f92 {number} defines the amount of fuel in the tank
 * @param f95 {number} defines the amount of fuel in the tank
 * @param fdiesel {number} defines the amount of fuel in the tank
 * @param limit {number} defines capacity of fuel station.
 * @constructor
 */
function fuelContracts(f92, f95, fdiesel, limit) {
    if (typeof f92 != 'number' && typeof f92 != 'number' && typeof f92 != 'number' && typeof f92 != 'number') {
        throw 'Please enter valid data in contract!'
    }

    this.f92 = f92 || 0;
    this.f95 = f95 || 0;
    this.fdiesel = fdiesel || 0;
    this.limit = limit || 1000;

    this.addFuel = function (fuelType) {
        switch (fuelType) {
            case FUEL_TYPES.F92:
                while (this.f92 < this.limit) {
                    this.f92 += 100;
                }
                break;
            case FUEL_TYPES.F95:
                while (this.f95 < this.limit) {
                    this.f95 += 100;
                }
                break;
            case FUEL_TYPES.FDIESEL:
                while (this.fdiesel < this.limit) {
                    this.fdiesel += 100;
                }
                break;
            default:
                throw 'I do not know this kind of fuel! Sorry!'
        }
        return 'Thank you for signed a contract';
    };
    this.subtract = function (fuelType, fuelValue) {
        switch (fuelType) {
            case FUEL_TYPES.F92:
                this.f92 -= fuelValue;
                break;
            case FUEL_TYPES.F95:
                this.f95 -= fuelValue;
                break;
            case FUEL_TYPES.FDIESEL:
                this.fdiesel -= fuelValue;
                break;
            default:
                throw 'I do not know this kind of fuel! Sorry!'
        }
    };
}